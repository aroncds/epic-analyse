import json
import time
import asyncio

from db import Block, Session
from helpers.requests import fetch, bulk_request, request


NODE_URL = "http://127.0.0.1:3413/"


def get_exists(session, response):
    hashs = [data["header"]["hash"] for data in response]
    blocks = session.query(Block)\
        .filter(Block.hash.in_(hashs)).all()
    return [block.hash for block in blocks]


def parse_block(response, orphan=False, failed=False):
    block_header = response["header"]
    return Block(
        hash=block_header["hash"],
        height=block_header["height"],
        timestamp=block_header["timestamp"],
        total_difficulty=sum(block_header["total_difficulty"].values()),
        is_orphan=orphan,
        is_failed=failed
    )


async def get_main_blocks(session, height: int):
    return await fetch(f"{NODE_URL}/v1/blocks/{height}", session)


async def get_orphans_blocks(session):
    return await fetch(f"{NODE_URL}/v1/orphans", session)


async def get_failed_blocks(session):
    return await fetch(f"{NODE_URL}/v1/invalidblocks", session)


async def get_status(session):
    return await fetch(f"{NODE_URL}/v1/status", session)


async def run(length):
    print("Start make request block!")

    time_start = time.time()

    status = await request(get_status)
    current_height = status["tip"]["height"]

    # requests
    blocks = await bulk_request(range(current_height-length, current_height), get_main_blocks)
    orphan_blocks = await request(get_orphans_blocks)
    invalid_blocks = await request(get_failed_blocks)

    # database
    db = Session()

    block_exists = get_exists(db, blocks)
    blocks_ = [
        parse_block(response)
        for response in blocks
        if response["header"]["hash"] not in block_exists
    ]
    db.add_all(blocks_)
    db.commit()

    block_exists = get_exists(db, orphan_blocks)
    blocks_ = [
        parse_block(response, orphan=True)
        for response in orphan_blocks
        if response["header"]["hash"] not in block_exists
    ]
    db.add_all(blocks_)
    db.commit()

    block_exists = get_exists(db, invalid_blocks)
    blocks_ = [
        parse_block(response, failed=True)
        for response in invalid_blocks
        if response["header"]["hash"] not in block_exists
    ]
    db.add_all(blocks_)
    db.commit()

    db.close()
    print(f"Time elapsed: {time.time() - time_start}")


loop = asyncio.get_event_loop()
loop.run_until_complete(asyncio.ensure_future(run(6)))
