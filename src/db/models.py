from sqlalchemy import Column, Integer, String, Boolean

from .base import Base


__ALL__ = ['Block', ]


class Block(Base):
    __tablename__ = "blocks"

    hash = Column(String, primary_key=True)
    height = Column(Integer)
    timestamp = Column(Integer)
    is_orphan = Column(Boolean)
    is_failed = Column(Boolean)
    total_difficulty = Column(Integer)

    def __init__(self, hash, height, timestamp,
                 total_difficulty, is_orphan=False, is_failed=False):
        self.hash = hash
        self.height = height
        self.timestamp = timestamp
        self.is_orphan = is_orphan
        self.is_failed = is_failed
        self.total_difficulty = total_difficulty
