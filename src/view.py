from db import Block, Session


def select():
    session = Session()
    blocks = session.query(Block).order_by("height").all()

    for block in blocks:
        print(f"""
Block:
    Height: {block.height}
    Hash: {block.hash}
    Timestamp: {block.timestamp}
    Difficulty: {block.total_difficulty}
    Orphan: {block.is_orphan}
    Failed: {block.is_failed}
        """)
    
    session.close()

select()