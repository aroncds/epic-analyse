import json
import asyncio
from aiohttp import ClientSession


async def fetch(url, session) -> dict:
    async with session.get(url) as response:
        if response.status == 200:
            data = await response.read()
            return json.loads(data)


async def bulk_request(range, func):

    async with ClientSession() as session:
        tasks = [
            asyncio.ensure_future(func(session, h))
            for h in range
        ]

        responses = await asyncio.gather(*tasks)
    
    return responses


async def request(func):
    async with ClientSession() as session:
        return await func(session)
